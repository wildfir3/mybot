from handlerBase import HandlerBase, Command
import database
from datetime import datetime
import random

class ScratchHandler(HandlerBase):

	def __init__(self, levels):

		self.scratchLimit = 5
		self.scratchUsage = '!scratch <message>'
		self.scratchPadUsage = '!scratchpad [add|delete|clear] [message]'

		HandlerBase.__init__(self, levels, Command('!scratch', self.handleScratch, u'Adds a message to your personal \u0002!scratchpad\u0002 which you can view at a later date', self.scratchUsage))
		HandlerBase.__init__(self, levels, Command('!scratchpad', self.handleScratchPad, u'Use this command to view and edit your personal scratchpad. You can use \u0002!scratch <message>\u0002 as a quick way to add new items to your pad', self.scratchPadUsage))

	def handleScratch(self, message):
		if not self.isAuthedUser(message.sender):
			message.reply('To use your scratchpad your nickname must be registered and identified. This protects your personal scratchpad from being viewed or modified by others')
			self.sendRegistrationExplanation(message.sender)
			return
			
		if message.numArgs < 1:
			message.reply('usage: ' + self.scratchUsage)
			return

		self.addScratch(message.sender, message.joinFrom(0))

	def handleScratchPad(self, message):
		if not self.isAuthedUser(message.sender):
			self.sendRegistrationExplanation(message.sender)
			return
			
		if message.numArgs >= 1:
			action = message.args[0]
			if action == 'clear':
				self.clearScratchPad(message.sender)
				message.reply('Your scratchpad has been cleared')
			elif action == 'add':
				if message.numArgs <= 1:
					message.reply('You haven\'t entered a message to add to the scratchpad')
					return
				self.addScratch(message.sender, message.joinFrom(1))
			elif action == 'delete' or action == 'del':
				if message.numArgs <= 1:
					message.reply('You haven\'t specified an item number to delete')
					return
				scratches = self.getScratches(message.sender)
				index = message.args[1]
				if not self.isNumber(index):
					message.reply(index + ' is not a number')
					return
				if not self.isValidIndex(index, scratches):
					message.reply('No item matching that index')
					return
				scratches[int(index) - 1].delete()
				message.reply('Item removed successfully')

		else:
			scratches = self.getScratches(message.sender)
			if len(scratches) == 0:
				message.reply(u'You haven\'t got any scratch entries. You can add one with \u0002!scratch <message>\u0002')
				return

			messages = [u'Here are your scratch entries, newest first. Note the numbers below are used to remove entries e.g. \u0002!scratchpad delete 1\u0002 would remove your first entry']
			for index, value in enumerate(scratches):
				messages.append(u'\u0002%d:\u0002 %s' % (index + 1, value.message))
			message.reply(messages)

	def addScratch(self, user, message):
		if self.getScratchCount(user) >= self.scratchLimit:
			self.sendMsg(user, u'Sorry, you can only have a maximum of %d scratches. You can clear all entries using \u0002!scratchpad clear\u0002 or \u0002!scratchpad delete <number>\u0002 to delete a specific entry' % self.scratchLimit)
			if random.randint(0, 1) == 0:
				self.sendMsg(user, 'Is this limit restrictive? Please message WildFire and let him know')
			return

		Scratch.create(user, message)
		self.sendMsg(user, 'Scratch added')

	def getScratchCount(self, user):
		return database.execute('select count(*) from scratchpad where user = ?', (user.lower(),)).fetchone()[0]

	def getScratches(self, user):
		results = database.execute('select * from scratchpad where user = ? order by date desc', (user.lower(),)).fetchall()
		return [Scratch(r) for r in results]

	def clearScratchPad(self, user):
		database.execute('delete from scratchpad where user = ?', (user.lower(),))
		
class Scratch:
	def __init__(self, record):
		self.id  = record['id']
		self.user  = record['user']
		self.datetime = record['date']
		self.message = record['message']

	@staticmethod
	def create(user, message):
		database.execute('insert into scratchpad ([user], [date], [message]) values (?, ?, ?)', (user.lower(), datetime.now(), message))

	def delete(self):
		database.execute('delete from scratchpad where id = ?', (self.id,))

#settings
#delete on retrieve
#keep on retrieve
#order asc/desc

def __init__(levels):
	ScratchHandler(levels)
