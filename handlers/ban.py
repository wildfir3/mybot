import config
import irc
import ircCommands
import userTracker
from handlerBase import HandlerBase, Command

class BanHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels['ban'], Command('!ban', self.handleBan))
		HandlerBase.__init__(self, levels['unban'], Command('!unban', self.handleUnBan))

	def handleBan(self, message):
		destinationChannel = config.channel
		if message.numArgs == 2:
			destinationChannel = message.args[1]

		if message.numArgs >= 1:
			hostname = irc.getHostname(message.args[0])
			if hostname != None:
				if userTracker.isVoiced(message.args[0]):
					ircCommands.voice(message.args[0], config.channel, False)
				ircCommands.ban(hostname, destinationChannel, True)
		else:
			self.sendMsg(message.sender, 'usage: !ban <user> [channel]')

	def handleUnBan(self, message):
		destinationChannel = config.channel
		if message.numArgs == 2:
			destinationChannel = message.args[1]

		if message.numArgs >= 1:
			hostname = irc.getHostname(message.args[0])
			if hostname != None:
				ircCommands.ban(hostname, destinationChannel, False)
		else:
			self.sendMsg(message.sender, 'usage: !unban <user> [channel]')

def __init__(levels):
	BanHandler(levels)
