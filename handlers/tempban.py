import userTracker
from handlerBase import HandlerBase, Command
from datetime import datetime

class TempBanHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!tempban', self.handle))

	def handle(self, message):
		if message.numArgs >= 2:
			value = self.tryParseTime(message.sender, message.args[1])
			banee = message.args[0]

			if not isinstance(value, datetime):
				self.sendMsg(message.sender, 'NB: The preferred usage of this command is to specify username before the duration')
				value = self.tryParseTime(message.sender, message.args[0])
				banee = message.args[1]

			if not isinstance(value, datetime):
				self.sendMsg(message.sender, 'usage: !tempban <user> <duration>')
				self.sendMsg(message.sender, value)
				return

			userTracker.tempban(banee, value)
		else:
			self.sendMsg(message.sender, 'usage: !tempban <user> <duration>')

def __init__(levels):
	TempBanHandler(levels)
