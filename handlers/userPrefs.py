import config
from handlerBase import HandlerBase, Command
import handler
import database

class UserPrefsHandler(HandlerBase):
	def __init__(self, levels):
		self.usage = '!set <setting> <value>'
		HandlerBase.__init__(self, levels, Command('!set', self.handle, u'Allows you to set your personal preferences, for things such as whether you are replied to via PM or Notice. Type \u0002!set\u0002 on its own for a full listing of the available settings. You can also type \u0002!set\u0002 followed by the name of a setting for information on it', self.usage))
		handler.registerGlobalHandler('userPrefs', self)
		
	def handle(self, message):
		if message.numArgs >= 1:
			if self.isValidSetting(message.args[0]):
				if message.numArgs >= 2:
					value = ' '.join(message.args[1:])
					if self.isValidSettingOption(message.args[0], value):
						if self.isAuthedUser(message.sender):
							self.setSetting(message.args[0], message.sender, value)
							self.sendMsg(message.sender, 'Your ' + message.args[0] + ' setting is now \'' + value + '\'')
						else:
							self.sendRegistrationExplanation(message.sender)
					else:
						self.sendMsg(message.sender, 'Invalid value \'' + value + '\' for setting \'' + message.args[0] + '\'')
				else:
					msg = [self.getSettingHelp(message.args[0])]
					msg.append('Your ' + message.args[0] + u' setting is currently \u0002' + self.getSetting(message.args[0], message.sender))
					msg.append(self.getSettingOptions(message.args[0]))
					self.sendMultilineMsg(message.sender, msg)
			else:
				msg = ['Invalid setting \'' + message.args[0] + '\'']
				msg.append(u'Available settings: \u0002' + self.getSettings())
				self.sendMultilineMsg(message.sender, msg)
		else:
			msg = ['usage: ' + self.usage]
			msg.append(u'Available settings: \u0002' + self.getSettings())
			self.sendMultilineMsg(message.sender, msg)

	defaultSettings = {
	'msg': 'pm',
	'games': 'pm',
	'listnotify' : 'off',
	'reminders': 'pm',
	'tell': 'pm',
	'whois': 'not set'
	}

	settingOptions = {
	'msg': ['pm', 'notice'],
	'games': ['pm', 'notice'],
	'listnotify': ['off', 'games', 'events', 'all'],
	'reminders': ['pm', 'notice'],
	'tell': ['pm', 'notice', 'off'],
	'whois': None
	}

	settingHelp = {
	'msg': 'Choose how the bot responds to your commands: via PM, or via notices',
	'games': 'Choose how the bot notifies you about people wanting to play games: via PM, or via notices',
	'listnotify': 'Set this if you want to be notified when new lists are created (so you don\'t miss them whilst away/busy)',
	'reminders': 'Choose how the bot notifies you about reminders you have set for yourself: via PM, or via notices',
	'tell': 'Choose how the bot notifies you about messages which people have left you. Note: you can opt-out of receiving messages by setting tell to \'off\'',
	'whois': u'You can use whois to set information about yourself (e.g. facebook link) which other people will be able to view by typing \u0002!whois\u0002 followed by your name'
	}

	def getSetting(self, setting, user):
		curVal = self.settingExists(setting, user)
		if curVal != None:
			return curVal[0]
		return self.defaultSettings[setting]

	def setSetting(self, setting, user, value):
		value = value.lower()
		curVal = self.settingExists(setting, user)
		if curVal != None:
			database.execute('update userprefs set value=? where key=? and user=?', (value, setting, user.lower()))
		else:
			database.execute('insert into userprefs values (?,?,?)', (user.lower(), setting, value))

	def isValidSetting(self, setting):
		if setting.lower() in self.settingOptions.keys():
			return True
		return False

	def isValidSettingOption(self, setting, value):
		options = self.settingOptions[setting]
		if options == None or value.lower() in options:
			return True
		return False

	def getSettingOptions(self, setting):
		options = self.settingOptions[setting]
		if options == None:
			return 'This is a free text setting. You can enter whatever you like.'
		return 'The available settings for ' + setting + u' are: \u0002' + ' '.join(options)

	def getSettingHelp(self, setting):
		return self.settingHelp[setting]

	def getSettings(self):
		return ' '.join(self.settingOptions.keys())

	def settingExists(self, setting, user):
		value = database.execute('select value from userprefs where key=? and user=?', (setting,user.lower()))
		return value.fetchone()

	def getPeopleWhoWantInformingAboutLists(self):
		people = database.execute('select user, value from userPrefs where key=? and value<>?', ('listnotify', 'off')).fetchall()
		return dict([(x['user'], x['value']) for x in people])

	def getMsgPref(self, pref, user):
		msgPref = self.getSetting(pref, user)
		if msgPref == 'pm':
			return 'PRIVMSG'
		else:
			return 'NOTICE'

def __init__(levels):
	UserPrefsHandler(levels)
