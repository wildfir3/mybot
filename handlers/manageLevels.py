import config
import ircCommands
from handlerBase import HandlerBase, Command

class ManageLevelsHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels['op'], Command('!op', self.handleOp))
		HandlerBase.__init__(self, levels['deop'], Command('!deop', self.handleDeop))
		HandlerBase.__init__(self, levels['hop'], Command('!hop', self.handleHop))
		HandlerBase.__init__(self, levels['dehop'], Command('!dehop', self.handleDehop))
		HandlerBase.__init__(self, levels['voice'], Command('!voice', self.handleVoice))
		HandlerBase.__init__(self, levels['devoice'], Command('!devoice', self.handleDeVoice))

	def doLevel(self, message, level, give):
		if message.numArgs == 2:
			ircCommands.sendCommand(message.args[1], message.args[0], level, give)
		elif message.numArgs == 0:
			ircCommands.sendCommand(message.sender, message.destination, level, give)
		else:
			self.sendMsg(message.sender, 'usage: ' + message.command + ' <channel> <user>')

	def handleVoice(self, message):
		self.doLevel(message, 'v', True)

	def handleDeVoice(self, message):
		self.doLevel(message, 'v', False)

	def handleOp(self, message):
		self.doLevel(message, 'o', True)
	
	def handleDeop(self, message):
		self.doLevel(message, 'o', False)
	
	def handleHop(self, message):
		self.doLevel(message, 'h', True)
	
	def handleDehop(self, message):
		self.doLevel(message, 'h', False)

def __init__(levels):
	ManageLevelsHandler(levels)
