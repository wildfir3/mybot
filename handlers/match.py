import config
import database
import irc
import scheduler
from steamCommunity import SteamCommunity
import timeUtils
from handlerBase import HandlerBase, Command
from datetime import datetime, timedelta

matchModes = ['highlander']
fullTeam = 9
numberToClassName = {
	0: 'sub',
	1: 'scout',
	2: 'soldier',
	3: 'pyro',
	4: 'demoman',
	5: 'heavy',
	6: 'engineer',
	7: 'medic',
	8: 'sniper',
	9: 'spy'
}
nameToClassNumber = dict([[v[1], v[0]] for v in numberToClassName.items()])

class MatchChannel:
	def __init__(self, channel):
		self.channel = channel
		row = database.execute('select * from matchChannels where channel LIKE ?', (channel, )).fetchone()
		if row is not None:
			self.exists = True
			self.id = row['id']
			self.mode = row['mode']
			self.teamSize = row['teamSize']
		else:
			self.exists = False

	def exists(self):
		return self.exists

	@staticmethod
	def create(channelName, mode, teamSize):
		database.execute('insert into matchChannels (channel, mode, teamSize) VALUES (?,?,?)', (channelName, mode, teamSize))

class Match:
	def __init__(self, record):
		self.id = record['id']
		self.channelId = record['channelId']
		matchInfo = database.execute('select * from matchChannels where id = ?', (self.channelId, )).fetchone()
		self.channel = matchInfo['channel']
		self.teamSize = matchInfo['teamSize']
		self.matchId  = record['matchId']
		self.datetime = record['date']
		self.description = record['description']
		self.eventId = record['eventId']
		players = database.execute('select * from matchsignups where matchid = ?', (self.id,)).fetchall()
		self.players = dict([(r['class'], r['user']) for r in players if r['class'] != 0])
		self.subs = [r['user'] for r in players if r['class'] == 0]

		self.steamGroup = '0v0'
		self.steamUsername = 'COGSBot'
		self.steamPassword = 'annarulez'

	@staticmethod
	def create(channelId, matchId, date, description):
		database.execute('insert into matches([channelId], [matchId], [date], [description]) values (?, ?, ?, ?)', (channelId, matchId, date, description))
		match = Match.load(channelId, matchId)
		match.createSteamCommunityEvent()
		match.addScheduledNotifications()
		return match

	@staticmethod
	def load(channelId, matchId):
		match = database.execute('select * from matches where channelId = ? and matchId = ?', (channelId, matchId)).fetchone() 
		return None if match == None else Match(match)

	@staticmethod
	def reminder(channelId, matchId, hoursRemaining):
		match = Match.load(channelId, matchId)
		match.remindChannel(hoursRemaining)
	
	def getRemindTimes(self):
		return [(self.datetime + timedelta(days = -1), 24), (self.datetime + timedelta(hours = -6), 6), (self.datetime + timedelta(hours = -1), 1)]
		#return [(self.datetime + timedelta(days = 1), 24), (self.datetime + timedelta(hours = 1), 1)]

	def updateTime(self, newTime):
		self.removeScheduledNotifications()
		self.datetime = newTime
		self.addScheduledNotifications()
		self.modifySteamCommunityEventDate()
		
	def addScheduledNotifications(self):
		for time in self.getRemindTimes():
			if time[0] > datetime.now():
				scheduler.addEvent(time[0], [Match.reminder, self.channelId, self.matchId, time[1]])

	def removeScheduledNotifications(self):
		for time in self.getRemindTimes():
			if time[0] > datetime.now():
				scheduler.removeScheduledEvent(time[0], [Match.reminder, self.channelId, self.matchId, time[1]])
	
	def createSteamCommunityEvent(self):
		steam = SteamCommunity(self.steamUsername, self.steamPassword)
		eventId = steam.eventSpecific(self.steamGroup, '0v0 TF2 !match ' + str(self.matchId), 'GameEvent', self.description, self.datetime, 440)
		database.execute('update matches set [eventId] = ? where id = ?', (eventId, self.id))

	def modifySteamCommunityEventDate(self):
		if self.eventId == None:
			return
		steam = SteamCommunity(self.steamUsername, self.steamPassword)
		steam.eventSpecificModify(self.steamGroup, self.eventId, '0v0 TF2 !match ' + str(self.matchId), 'GameEvent', self.description, self.datetime, 440)

	def deleteSteamCommunityEvent(self):
		if self.eventId == None:
			return
		steam = SteamCommunity(self.steamUsername, self.steamPassword)
		steam.eventDelete(self.steamGroup, self.eventId)

	def delete(self):
		database.execute('delete from matches where id = ?', (self.id, ))
		database.execute('delete from matchsignups where matchId = ?', (self.id,))
		self.removeScheduledNotifications()
		self.deleteSteamCommunityEvent()

	def getMissingPlayers(self):
		return [numberToClassName[playerNum] for playerNum in numberToClassName.keys() if playerNum not in self.players.keys()]

	def getPlayerCount(self):
		return len(self.players.keys())

	def isFull(self):
		return self.getPlayerCount() == self.teamSize

	def getUserPosition(self, user):
		for item in self.players.items():
			if item[1].lower() == user.lower():
				return numberToClassName[item[0]]

		return numberToClassName[0]

	def userIsInMatch(self, user):
		lowerUser = user.lower()
		return lowerUser in [p.lower() for p in self.players.values()] or lowerUser in [s.lower() for s in self.subs]

	def positionIsTaken(self, position):
		positionId = self.getUserPositionForDB(position)
		if positionId == 0:
			return False
		return positionId in self.players.keys()

	def addUserToMatch(self, user, position):
		database.execute('insert into matchsignups values (?, ?, ?)', (self.id, user, self.getUserPositionForDB(position)))

	def removeUserFromMatch(self, user):
		database.execute('delete from matchsignups where matchId = ? and user LIKE ?', (self.id, user))

	def getUserAtPosition(self, position):
		return self.players[self.getUserPositionForDB(position)]

	def getUserPositionForDB(self, position):
		if type(position) == 'int':
			return position

		if position.isdigit():
			return int(position)

		return nameToClassNumber[position.lower()]

	def printClasses(self):
		output = ['(']
		for i in range(1, len(numberToClassName.keys())):
			if i in self.players.keys():
				output.append('#')
			else:
				output.append('-')
			if i % 3 == 0 and i < 9:
				output.append(' ')

		output.append(')')
		return ''.join(output)

	def remindChannel(self, hoursRemaining):
		irc.sendMsg(self.channel, '%dh remaining until %s' % (hoursRemaining, self.prettyPrint(withTime = False)))

	def prettyPrint(self, withTime = True):
		dayDateAndTime = ' %s %s' % (self.datetime.strftime('%A'), timeUtils.getFriendlyTime(self.datetime)) if withTime else ' '

		retVal = u'\u0002!match %d\u0002:%s %s [%d/%d players] (%d subs) %s' % (self.matchId, dayDateAndTime, self.description, self.getPlayerCount(), self.teamSize, len(self.subs), self.printClasses())
		retVal = retVal + (u' \u0002[FULL]\u0002' if self.isFull() else '')
		return retVal

	def detailPrint(self):
		retVal = [self.prettyPrint()]
		if len(self.players) > 0:
			playerKeys = self.players.keys()
			playerKeys.sort()
			neededKeys = [key for key in numberToClassName.keys() if key not in playerKeys]
			players = []
			needed = []
			for i in range(0, len(playerKeys)):
				if i % 2 == 1:
					colour = '04'
				else:
					colour = '07'

				players.append(u'\u0003%s\u0002%s\u0002: %s\u0003' % (colour, numberToClassName[playerKeys[i]], self.players[playerKeys[i]]))

			for key in neededKeys:
				if key != 0:
					needed.append(numberToClassName[key])

			retVal.append('Players: ' + ' | '.join(players))
			if len(needed) > 0:
				retVal.append(u'Unfilled: \u0002%s\u0002' % ', '.join(needed))

		if len(self.subs) > 0:
			retVal.append('Subs: ' + ', '.join(self.subs))
		return retVal


class MatchHandler(HandlerBase):

	def __init__(self, levels):
		self.matchInitUsage = '!matchinit <channel> <mode> <teamsize>'
		self.matchUsage = '!match <matchId> enrol|withdraw [class]'
		self.matchAddUsage = '!matchadd <[date] <time>> <description>'
		self.matchDelUsage = '!matchdel <matchId>'
		self.matchUpdUsage = '!matchupd <matchId> playeradd|playerdel|date|time|descr <...>' 

		HandlerBase.__init__(self, levels['matchadmin'], Command('!matchinit', self.handleMatchInit, u'Causes the named channel to become a match channel', self.matchInitUsage))
		HandlerBase.__init__(self, levels['match'], Command('!match', self.handleMatch, u'Allows you view the list of players in a match, as well as enrol or withdraw from a particular match. If you just want to see who\'s playing, specify the matchId only, e.g. \u0002!match 1\u0002', self.matchUsage))
		HandlerBase.__init__(self, levels['matchadmin'], Command('!matchadd', self.handleMatchAdd, 'Allows you to add matches', self.matchAddUsage))
		HandlerBase.__init__(self, levels['matchadmin'], Command('!matchdel', self.handleMatchDel, 'Allows you to remove matches', self.matchDelUsage))
		HandlerBase.__init__(self, levels['matchadmin'], Command('!matchupd', self.handleMatchUpd, 'Allows you to alter match info', self.matchUpdUsage))
		HandlerBase.__init__(self, levels['match'], Command('!matches', self.handleMatches, 'Allows you view a list of all matches in the system', '!matches'))

		self.listen('onConnected', self.__onConnected__)

	def __onConnected__(self):
		channels = map(lambda c: c['channel'], database.execute('select channel from matchChannels', '').fetchall())
		for channel in channels:
			irc.send('JOIN ' + channel)

	def handleMatchInit(self, message):
		if message.numArgs < 3:
			self.sendMsg(message.sender, self.matchInitUsage)
			return

		if not message.args[0].startswith('#'):
			self.sendMsg(message.sender, message.args[0] + ' is not a valid channel name')
			return

		if message.args[1] not in matchModes:
			self.sendMsg(message.sender, message.args[1] + ' is not a valid mode. Available modes: ' + ', '.join(matchModes))
			return

		if not self.isNumber(message.args[2]):
			self.sendMsg(message.sender, message.args[2] + ' team size must be a number')
			return

		if MatchChannel(message.args[0]).exists:
			self.sendMsg(message.sender, 'This channel is already a match channel')
			return

		MatchChannel.create(message.args[0], message.args[1], int(message.args[2]))
		irc.send('JOIN ' + message.args[0])
		self.sendMsg(message.args[0], u'This channel will now support matches. You can type \u0002!help match\u0002 and \u0002!help matches\u0002 to learn how to use the match commands.')


	def handleMatch(self, message):
		channel = MatchChannel(message.destination)
		if not self.validateMatchCommandChannel(message, channel):
			return

		matchIdExplanation = u'You can use \u0002!matches\u0002 to get a list of match IDs'
		usage = ['usage: ' + self.matchUsage, matchIdExplanation]
		if message.numArgs < 1:
			message.reply(usage)
			return

		matchId = message.args[0]
		if matchId == 'next':
			match = self.getNextMatch(channel.id)
			if match == None:
				message.reply('There are no future matches!')
				return
		elif matchId == 'tomorrow':
			match = self.getNextMatchTomorrow(channel.id)
			if match == None:
				message.reply('There is no match tomorrow!')
				return
		else:
			match = self.getMatch(channel.id, matchId)

		if match == None:
			message.reply(['The match with id \'%s\' does not exist' % matchId, matchIdExplanation])
			return
		
		if message.numArgs >= 2:
			userIsInMatch = match.userIsInMatch(message.sender)
			if message.args[1] == 'withdraw':
				if not userIsInMatch:
					message.reply('You are not in this match - did you mean to enrol?')
					return
				match.removeUserFromMatch(message.sender)
				message.reply('You have been removed from the match')
			elif message.args[1].startswith('enrol'):
				if userIsInMatch:
					message.reply('You are already in this match (playing as %s)' % match.getUserPosition(message.sender))
					return
				if message.numArgs != 3:
					message.reply(['You must state the position you wish to enrol for', self.matchUsage])
					return

				userPosition = message.args[2]
				if not self.isValidUserPosition(userPosition):
					message.reply('\'%s\' is not a valid position to enrol for. The available positions are: %s' % (userPosition, ', '.join(match.getMissingPlayers())))
					return

				if match.positionIsTaken(userPosition):
					message.reply('This position is already taken by %s' % match.getUserAtPosition(userPosition))
					return

				match.addUserToMatch(message.sender, userPosition)
				message.reply('You have been enrolled in the match')
			else:
				message.reply(usage)
				return
		else:
			message.reply(match.detailPrint())
			return

		if message.args[1] == 'withdraw':
#			self.informChannelMatchChanged('%s just withdrew from' % message.sender, match, basic = True)
			pass
		else:
			self.informChannelMatchChanged('%s just enrolled in' % message.sender, match, basic = True)


	def handleMatchAdd(self, message):
		channel = MatchChannel(message.destination)
		if not self.validateMatchCommandChannel(message, channel):
			return

		if message.numArgs < 2:
			self.sendMsg(message.sender, self.matchAddUsage)
			return

		dateParser = self.parseDateTime(message.args[0], message.args[1])
		if not dateParser.success:
			self.sendMsg(message.sender, dateParser.error)
			return
		
		match = Match.create(channel.id, self.getNextMatchId(channel.id), dateParser.value, message.joinFrom(dateParser.argsUsed))
		self.sendMsg(message.destination, message.sender + u' just added a match. \u0002!match %d\u0002 to view the match details' % match.matchId)

			
	def handleMatchDel(self, message):
		channel = MatchChannel(message.destination)
		if not self.validateMatchCommandChannel(message, channel):
			return

		if message.numArgs != 1:
			self.sendMsg(message.sender, self.matchDelUsage)
			return

		if not self.matchWithIdExists(channel.id, message.args[0]):
			self.sendMsg(message.sender, 'The match with id \'%s\' does not exist' % message.args[0])
			return

		match = self.getMatch(channel.id, message.args[0])
		match.delete()
		self.sendMsg(message.sender, 'Match deleted successfully')
			
	def handleMatchUpd(self, message):
		channel = MatchChannel(message.destination)
		if not self.validateMatchCommandChannel(message, channel):
			return

		if message.numArgs < 3:
			self.sendMsg(message.sender, self.matchUpdUsage)
			return

		matchId = message.args[0]
		if not self.matchWithIdExists(channel.id, matchId):
			self.sendMsg(message.sender, 'The match with id \'%s\' does not exist' % matchId)
			return
		match = self.getMatch(channel.id, matchId)

		update = message.args[1]
		if update == 'playeradd' or update == 'playerdel':
			user = message.args[2]
			userIsInMatch = match.userIsInMatch(user)
				
			if update == 'playeradd':
				if message.numArgs < 4:
					self.sendMsg(message.sender, self.matchUpdUsage)
					return

				if userIsInMatch:
					self.sendMsg(message.sender, '%s is already in this match (playing as %s)' % (user, match.getUserPosition(user)))
					return
				position = message.args[3]

				if not self.isValidUserPosition(position):
					self.sendMsg(message.sender, '\'%s\' is not a valid position to enrol for' % position)
					return

				if match.positionIsTaken(position):
					self.sendMsg(message.sender, 'This position is already taken by %s' % match.getUserAtPosition(position))
					return

				match.addUserToMatch(user, position)
			elif update == 'playerdel':
				if not userIsInMatch:
					self.sendMsg(message.sender, '%s is not in this match' % user)
					return
				match.removeUserFromMatch(user)
		elif update == 'time':
			time = self.tryParseTime(message.sender, message.args[2])
			if not isinstance(time, datetime):
				self.sendMsg(message.sender, time)
				return
			matchTime = timeUtils.merge(match.datetime, time)
			if not isinstance(matchTime, datetime):
				self.sendMsg(message.sender, time)
				return
			match.updateTime(matchTime)
			database.execute('update matches set [date] = ? where id = ?', (matchTime, match.id))
		elif update == 'date':
			date = self.tryParseDate(message.sender, message.args[2])
			if not isinstance(date, datetime):
				self.sendMsg(message.sender, date)
				return
			matchTime = timeUtils.merge(date, match.datetime)
			if not isinstance(matchTime, datetime):
				self.sendMsg(message.sender, time)
				return
			match.updateTime(matchTime)
			database.execute('update matches set [date] = ? where id = ?', (matchTime, match.id))
		elif update == 'descr':
			database.execute('update matches set [description] = ? where id = ?', (' '.join(message.args[2:]), match.id))
		else:
			self.sendMsg(message.sender, '%s is not a valid update action' % update)
			return

		self.sendMsg(message.sender, 'Match updated successfully')
		#self.informChannelMatchChanged('%s just updated' % message.sender, match, basic = True)


	def validateMatchCommandChannel(self, message, channel):
		if not message.isChannelDestination():
			self.sendMsg(message.sender, 'This command can only be typed directly into channels which have match support')
			return False
		channel = MatchChannel(message.destination)
		if not channel.exists:
			self.sendMsg(message.sender, 'This channel does not currently have match support enabled')
			return False
		return True
		

	def handleMatches(self, message):
		channel = MatchChannel(message.destination)
		if not self.validateMatchCommandChannel(message, channel):
			return

		matches = [match.prettyPrint() for match in self.getMatches(channel.id, resultLimit = 6)]
		if len(matches) == 0:
			self.sendMsg(message.sender, 'There are no upcoming matches at the moment')
		else:
			self.sendMultilineMsg(message.sender, matches)

	def getNextMatch(self, channelId):
		matches = self.getMatches(channelId, resultLimit = 1)
		if len(matches) == 0:
			return None
		return matches[0]

	def getNextMatchTomorrow(self, channelId):
		record = database.execute('select * from matches where date([date]) = date(DATETIME("now", "localtime", "+1 days")) and channelId = ? order by [date] asc limit 1', (channelId, )).fetchone()
		if record == None:
			return None
		return Match(record)

	def getMatches(self, channelId, includePastMatches = False, resultLimit = None):
		dateClause = '' if includePastMatches else 'STRFTIME("%s", [date]) > STRFTIME("%s", (DATETIME("now", "localtime", "-1 hour"))) and'
		limitClause = '' if resultLimit == None else ' LIMIT ' + str(resultLimit)
		return [Match(record) for record in database.execute('select * from matches where ' + dateClause + ' channelId = ? order by [date] asc' + limitClause, (channelId, )).fetchall()]

	def getMatch(self, channelId, matchId):
		return Match.load(channelId, matchId)

	def getNextMatchId(self, channelId):
		matches = self.getMatches(channelId, includePastMatches = True)
		if len(matches) == 0:
			return 1
		matchIds = [match.matchId for match in matches]
		matchIds.sort(reverse = True)
		return matchIds[0] + 1

	def matchWithIdExists(self, channelId, matchId):
		return self.getMatch(channelId, matchId) != None

	def isValidUserPosition(self, position):
		if position.isdigit():
			return int(position) in numberToClassName.keys()
			
		return position.lower() in nameToClassNumber.keys()

	def informChannelMatchChanged(self, changeExplanation, match, basic = False):
		match = self.getMatch(match.channelId, match.matchId)
		if basic:
			message = [match.prettyPrint()]
		else:
			message = match.detailPrint()

		message[0] = changeExplanation + ' ' + message[0]
		self.sendMultilineMsg(match.channel, message)

def __init__(levels):
	MatchHandler(levels)
