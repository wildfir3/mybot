import random
from handlerBase import HandlerBase, Command

class VuvuzelaHandler(HandlerBase):

	def __init__(self, levels):
		self.chance = 1
		HandlerBase.__init__(self, levels, Command('!vuvuzela', self.handle, 'Take a guess...', '!vuvuzela'))

	def handle(self, message):
		destination = message.destination if message.destination.startswith('#') else message.sender
		if self.isAuthedUserAtLevel(message.sender, 'committee'):
			self.buzz(destination)
		elif random.randint(0, self.chance) == 0:
			if self.chance < 100:
				self.chance += 3
			self.buzz(destination)

	def buzz(self, destination):
		if random.randint(0, 5) == 0:
			self.sendMsg(destination, "Hey, do you wanna hear the most annoying sound in the world?")
		else:
			self.inform(destination, "BBBBZZZZZZZZZZZZZZZZZZ") 

def __init__(levels):
	VuvuzelaHandler(levels)
