import commandHandler
from handlerBase import HandlerBase, Command

class QuitHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!quit', self.handle))

	def handle(self, message):
		commandHandler.handleQuit(message)

def __init__(levels):
	QuitHandler(levels)
