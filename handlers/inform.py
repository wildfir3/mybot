import config
from handlerBase import HandlerBase, Command
import handler

class InformHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!inform', self.handle))
		handler.registerGlobalHandler('inform', self)

	def handle(self, message):
		if message.numArgs >= 2:
			information = ' '.join(message.args[1:])
			self.informPotentialUserGroup(message.args[0], information)

		else:
			self.inform(message.sender, 'usage: !inform <destination> <message>')

	def informPotentialUserGroup(self, group, information):
		usergroups = self.getHandler('usergroups')
		if group in usergroups.getGroups():
			for person in usergroups.getUsersInGroup(group):
				s = '' if group.endswith('e') else 's'
				self.inform(person, information, target = ' (all ' + group + s + ')')
		else:
			self.inform(group, information)

def __init__(levels):
	InformHandler(levels)
