import config
from datetime import datetime
import database
from handlerBase import HandlerBase, Command
import handler

class UsergroupHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!groupadd', self.handleGroupAdd))
		HandlerBase.__init__(self, levels, Command('!groupdel', self.handleGroupDel))
		handler.registerGlobalHandler('usergroups', self)

	def handleGroupAdd(self, message):
		if self.validate(message, 'add'):
			self.addUserToGroup(message.sender, message.args[0], message.args[1])

	def handleGroupDel(self, message):
		if self.validate(message, 'del'):
			self.removeUserFromGroup(message.sender, message.args[0], message.args[1])

	def validate(self, message, operation):
		if message.numArgs == 0:
			self.sendMsg(message.sender, 'usage: !group%s <user> <group>' % operation)
			return False
		if message.numArgs == 1:
			self.sendMsg(message.sender, '%s is in the following groups: %s' % (message.args[0], ' '.join(self.getUserGroups(message.args[0]))))
			self.sendMsg(message.sender, 'usage: !group%s <user> <group>' % operation)
			return False
		return True

	def userIsInGroup(self, user, group):
		return len(database.execute('select user from usergroups where user = ? and [group] = ?', (user.lower(), group.lower())).fetchall()) > 0

	def addUserToGroup(self, sender, user, group):
		if not self.userIsInGroup(user, group):
			database.execute('insert into usergroups values (?, ?)', (user.lower(), group.lower()))
			self.sendMsg(sender, 'user added to group succesfully')
		else:
			self.sendMsg(sender, '%s is already in the %s group' % (user, group))

	def removeUserFromGroup(self, sender, user, group):
		if self.userIsInGroup(user, group):
			database.execute('delete from usergroups where user = ? and [group] = ?', (user.lower(), group.lower()))
			self.sendMsg(sender, 'user removed from group successfully')
		else:
			self.sendMsg(sender, '%s is not in the %s group. Nothing to do.' % (user, group))

	def getUserGroups(self, user):
		return [item[0] for item in database.execute('select [group] from usergroups where user = ?', (user.lower(),)).fetchall()]

	def getGroups(self):
		groups = database.execute('select distinct [group] from usergroups', '').fetchall()
		return [item[0] for item in groups]

	def getUsersInGroup(self, group):
		return [item[0] for item in database.execute('select distinct [user] from usergroups where [group] = ?', (group,)).fetchall()] 

def __init__(levels):
	UsergroupHandler(levels)
