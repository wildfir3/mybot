from handlerBase import HandlerBase, Command
import userTracker

class SeenHandler(HandlerBase):

	def __init__(self, levels):
		self.usage = '!seen <nickname>'
		HandlerBase.__init__(self, levels, Command('!seen', self.handle, u'If someone is offline or not responding, use this to find out when they were last online or how long since they last spoke. If they\'re offline, you might like to leave them a message with \u0002!tell\u0002', self.usage))

	def handle(self, message):
		if message.numArgs == 1:
			self.sendMsg(message.sender, userTracker.getLastSeen(message.args[0]))
		else:
			self.sendMsg(message.sender, 'usage: ' + self.usage)

def __init__(levels):
	SeenHandler(levels)
