import constants
from handlerBase import HandlerBase, Command

class ServerHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!server', self.handle, 'Information about the servers COGS has running', '!server'))

	def handle(self, message):
		self.sendMsg(message.sender, constants.getConstant('serverInfo'))

def __init__(levels):
	ServerHandler(levels)
