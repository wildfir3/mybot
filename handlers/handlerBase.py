import handler
import config
import constants
import irc
import timeUtils
import userTracker
import re

pattern = re.compile('[a-z\[\]\\`_\^{|}][a-z0-9\[\]\\`_\^{|}-]*', re.I)

class Command:
	def __init__(self, trigger, handler, help = 'Detailed help is unavilable for this command', usage = 'Usage info is unavailable for this command'):
		self.trigger = trigger
		self.handler = handler
		self.help = help
		self.usage = usage

class HandlerBase:
	def __init__(self, level, command):
		handler.addHandler(level, command)

	def sendMsg(self, destination, content, msgType = None, evenIfOffline = False):
		irc.sendMsg(destination, content, msgType, evenIfOffline)

	def inform(self, destination, message, target = ''):
		irc.inform(destination, message, target) 

	def sendMultilineMsg(self, destination, messages, msgType = None):
		irc.sendMultilineMsg(destination, messages, msgType)

	def sendRegistrationExplanation(self, sender):
		irc.sendMsg(sender, constants.getConstant('registrationHelp'))

	def isAuthedUser(self, user):
		return userTracker.isAuthedUser(user)

	def isAuthedUserAtLevel(self, user, level):
		return level in self.getLevelsForUser(user) and userTracker.isAuthedUser(user)

	def tryParseTime(self, sender, input):
		return timeUtils.getTimeInFuture(input)

	def tryParseDate(self, sender, input):
		return timeUtils.getDateInFuture(input)

	def parseDateTime(self, dateOrTime, time):
		return timeUtils.getDateTime(dateOrTime, time)

	def isValidNickname(self, name):
		return pattern.match(name) is not None

	@staticmethod
	def getLevelsForUser(user):
		usergroups = handler.getGlobalHandler('usergroups')
		return usergroups.getUserGroups(user)
	
	def getHandler(self, name):
		return handler.getGlobalHandler(name)

	def isNumber(self, string):
		try:
			val = int(string)
			return True
		except:
			pass
		return False
	
	def isValidIndex(self, index, list):
		try:
			val = int(index)
			if val > 0 and val <= len(list):
				return True
		except:
			pass
		return False

	def listen(self, action, handle):
		handler.listen(action, handle)
