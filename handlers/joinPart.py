import irc
import channelTracker
from handlerBase import HandlerBase, Command
import handler

class JoinPartHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!join', self.handleJoin))
		HandlerBase.__init__(self, levels, Command('!part', self.handlePart))
		HandlerBase.__init__(self, levels, Command('!modes', self.handleModes))

		handler.registerGlobalHandler('joinpart', self)

	def handleJoin(self, message):
		if message.numArgs == 1:
			self.joinChannel(message.args[0])
		else:
			self.sendMsg(message.sender, 'usage: !join <channel>')

	def joinChannel(self, channel):
		irc.send('JOIN ' + channel)
		channelTracker.join(channel)

	def handleModes(self, message):
		if message.numArgs >= 2:
			self.setChannelModes(message.args[0], ' '.join(message.args[1:]))
		else:
			self.sendMsg(message.sender, 'usage: !modes <channel> <modes>')

	def setChannelModes(self, channel, modes):
		irc.send('MODE ' + channel + ' ' + modes)

	def handlePart(self, message):
		if message.numArgs >= 1:
			self.partChannel(message.args[0], message.args[1:])
		else:
			self.sendMsg(message.sender, 'usage: !part <channel> [description]')

	def partChannel(self, channel, reason):
		reasonString = 'bye'
		if reason:
			reasonString = ' '.join(reason)
		irc.send('PART ' + channel + ' :' + reasonString)

def __init__(levels):
	JoinPartHandler(levels)
