import constants
from handlerBase import HandlerBase, Command

class StatsHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!stats', self.handle, 'A quick link to our IRC statistics generated from my logs', '!stats'))

	def handle(self, message):
		self.sendMsg(message.sender, constants.getConstant('stats'))

def __init__(levels):
	StatsHandler(levels)

