from handlerBase import HandlerBase, Command
import handler
import irc
import regex

currentTopics = {}
topicHelp = {
	'add': 'add an item to the end of the current topic',
	'del': 'remove a numbered item from the topic',
	'set': 'overwrite the topic with a new one (input may be pipe delimited)',
	'ins': 'insert an item into the topic before the numbered item',
	'ovr': 'overwrite a numbered item in the topic with a new value'
}

class TopicHandler(HandlerBase):

	def __init__(self, levels):
		HandlerBase.__init__(self, levels, Command('!topic', self.handleTopic))
		HandlerBase.__init__(self, levels, Command('!topicsave', self.handleTopicSave))

		self.maxTopicLength = 309
		handler.registerGlobalHandler('topic', self)

	def handleTopic(self, message):
		success = -1
		if message.numArgs >= 1:
			if not self.isValidChannel(message.args[0]):
				if message.args[0].startswith('#'):
					irc.send('TOPIC ' + message.args[0])
					self.sendMsg(message.sender, 'usage: !topic <channel>')
					self.sendMsg(message.sender, 'I don\'t know of a topic for channel \'' + message.args[0] + '\'. Trying to retrieve topic. Did you type the channel name correctly?')
				else:
					self.sendMsg(message.sender, message.args[0] + ' is not a valid channel name. Channel names must begin with a #')
				return
		else:
			self.sendMsg(message.sender, 'usage: !topic <channel> [<action> [number] <value>]')
			return

		if message.numArgs == 1:
			self.sendMsg(message.sender, 'The available topic actions are: ' + ' '.join(topicHelp.keys()))
			self.sendMsg(message.sender, 'Use the numbered parts below to modify the topic:')
			self.sendMsg(message.sender, self.getTopicParts(message.args[0]))
			return
		if message.numArgs >= 2:
			if message.args[1] in topicHelp.keys():
				if message.numArgs >= 3:
					if message.args[1] == 'ins' or message.args[1] == 'ovr':
						if message.numArgs >= 4:
							if message.args[1] == 'ins':
								success = self.insertItem(message.args[0], message.args[2], ' '.join(message.args[3:]))
							else:
								success = self.overwriteItem(message.args[0], message.args[2], ' '.join(message.args[3:]))
						else:
							self.sendMsg(message.sender, 'usage: !topic <channel> [<action> <number> <value>]')
					else:
						if message.args[1] == 'add':
							success = self.addItem(message.args[0], ' '.join(message.args[2:]))
						elif message.args[1] == 'del':
							success = self.removeItem(message.args[0], message.args[2])
						elif message.args[1] == 'set':
							success = self.setTopic(message.args[0], ' '.join(message.args[2:]))
				else:
					self.sendMsg(message.sender, 'usage: !topic <channel> [<action> [number] <value>]')
					self.sendMsg(message.sender, 'Use ' + message.args[1] + ' to ' + topicHelp[message.args[1]])
			else:
				self.sendMsg(message.sender, 'usage: !topic <channel> [<action> [number] <value>]')
				self.sendMsg(message.sender, 'The available topic actions are: ' + ' '.join(topicHelp.keys()))
		
		if success != -1:
			if success == None:
				self.sendMsg(message.sender, 'Topic was not changed. Did you have a pipe character in your new topic item? Pipe characters can only be used as separators.')
			elif success:
				self.sendMsg(message.sender, u'Topic changed (\u0002!topicsave ' + message.args[0] + u'\u0002 to save changes). The new topic will be:')
				self.sendMsg(message.sender, self.getCurrentTopic(message.args[0]))
				if len(self.getCurrentTopic(message.args[0])) >= self.maxTopicLength:
					self.sendMsg(message.sender, u'\u000304\u0002Warning:\u0003 topic length may be too long.\u0002 Saved topic may be truncated by the server! Suggest deletion/truncation of some topic parts')
			else:
				self.sendMsg(message.sender, u'Topic was not changed. The number must correspond to one of the topic sections. \u0002!topic ' + message.args[0] + u'\u0002 to view the section numbers') 

	def handleTopicSave(self, message):
		if message.numArgs == 1:
			if self.isValidChannel(message.args[0]):
				self.saveTopic(message.args[0])
			else:
				self.sendMsg(message.sender, 'usage: !topicsave <channel>')
				self.sendMsg(message.sender, 'I don\'t know of a topic for channel \'' + message.args[0] + '\'. Trying to retrieve topic. Did you type the channel name correctly?')
		else:
			self.sendMsg(message.sender, 'usage: !topicsave <channel>')

	def getCurrentTopic(self, channel):
		retVal = []
		for item in enumerate(currentTopics[channel]):
			if item[0] % 2 == 1:
				colour = '04'
			else:
				colour = '07'
			retVal.append(u'\u0003' + colour + currentTopics[channel][item[0]] + u'\u0003')
		return ' | '.join(retVal)

	def getTopicParts(self, channel):
		retVal = ''
		for item in enumerate(currentTopics[channel]):
			retVal += (u'\u0002' + str(item[0] + 1) + u'\u0002: ' + item[1] + ' ')
		return retVal

	def addItem(self, channel, item):
		if not self.isValidItem(item):
			return None
		item = regex.removeColourCodes(item)
		currentTopics[channel].append(item)
		return True

	def removeItem(self, channel, index):
		if self.isValidIndex(index, currentTopics[channel]):
			currentTopics[channel].pop(int(index) - 1)
			return True
		return False

	def insertItem(self, channel, index, item):
		if not self.isValidItem(item):
			return None
		if self.isValidIndex(index, currentTopics[channel]):
			item = regex.removeColourCodes(item)
			currentTopics[channel].insert(int(index) - 1, item)
			return True
		return False

	def overwriteItem(self, channel, index, item):
		if not self.isValidItem(item):
			return None
		if self.isValidIndex(index, currentTopics[channel]):
			item = regex.removeColourCodes(item)
			currentTopics[channel][int(index) - 1] = item
			return True
		return False

	def isValidItem(self, item):
		return not '|' in item

	def isValidChannel(self, channel):
		return channel in currentTopics.keys()

	def saveTopic(self, channel):
		topicToSet = self.getCurrentTopic(channel)
		irc.send('TOPIC ' + channel + ' :' + topicToSet)

	def setTopic(self, channel, newTopic):
		newTopic = regex.removeColourCodes(newTopic)
		currentTopics[channel] = map(lambda item: item.strip(), newTopic.split('|'))
		return True

def __init__(levels):
	TopicHandler(levels)
