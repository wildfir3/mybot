import database
from handlerBase import HandlerBase, Command
from validation import Validation
from context import ContextAware, ContextManager, RequiredProperty
from steamCommunity import SteamCommunity

class Event(ContextAware):
    def __init__(self, username, dataRecord = None):
        if dataRecord is None:
            ContextAware.__init__(self, username)

            self.requiredProperties = [
                RequiredProperty('name', 'What is the name of this event?', Validation.uniqueEventName),
                RequiredProperty('description', 'Enter a description of the event e.g. venue details', Validation.notEmpty),
                RequiredProperty('startDate', 'Date and time of event start', Validation.dateTime),
                RequiredProperty('endDate', 'Date and time of event end', Validation.dateTimeAfter(lambda : self.startDate)),
                RequiredProperty('steamEvent', 'Publish this event on Steam?', Validation.yesNo)
               #RequiredProperty('listCreation', 'Create a list for user signups?', Validation.yesNo)
            ]
        else:
            self.name = dataRecord['name']
            self.description = dataRecord['description']
            self.startDate = dataRecord['startDate']
            self.endDate = dataRecord['endDate']
            self.listName = dataRecord['listName']
            self.steamEventId = dataRecord['steamEventId']

        self.steam = SteamCommunity('cogsbot', 'X5f2VjX0')
        self.steamGroup = 'cogsbot'

    def delete(self):
        if self.steamEventId is not None:
            try:
                self.steam.eventDelete(self.steamGroup, self.steamEventId)
            except Exception, e:
                print e
                self.messageUser('Sorry, there was a problem removing this event from Steam.')

        database.execute('delete from event where [name] = ?', (self.name,))

    @staticmethod
    def load(eventName):
        event = database.execute('select * from event where [name] = ?', (eventName,)).fetchone() 
        if event is None:
            return None 
        else:
            return Event(None, dataRecord=event)

    def onComplete(self):
        self.messageUser('completed')
        steamEventId = None
        listName = ''

        if self.steamEvent:
            try:
                steamEventId = self.steam.eventSpecific(self.steamGroup, self.name, 'PartyEvent', self.description, self.startDate)
            except Exception, e:
                print e
                self.messageUser('Sorry, there was a problem adding this event to Steam.')

        database.execute('insert into event ([name], [description], [startDate], [endDate], [listName], [steamEventId]) values (?, ?, ?, ?, ?, ?)', (self.name, self.description, self.startDate, self.endDate, listName, steamEventId))

class EventHandler(HandlerBase):
    def __init__(self, levels):
        HandlerBase.__init__(self, levels, Command('!eventadd', self.handleEventAdd, 'Begin event creation process. You will be guided through the requirements interactively', '!eventadd'))
        HandlerBase.__init__(self, levels, Command('!eventdel', self.handleEventDel, 'Remove an event', '!eventdel <name>'))

    def handleEventAdd(self, message):
        event = ContextManager.getContext(message.sender)
        if event is None:
            event = Event(message.sender)
            event.askNextQuestion()
            ContextManager.setContext(message.sender, event)
        else:
            ContextManager.resumeContext(message.sender)

    def handleEventDel(self, message):
        if message.numArgs != 1:
            message.reply('Please provide the name of the event you want to delete')
            return

        event = Event.load(message.args[0])
        if event is None:
            message.reply('Cannot find event named \'%s\'' % message.args[0])
        else:
            event.delete()
            message.reply('Event deleted');

def __init__(levels):
    EventHandler(levels)
