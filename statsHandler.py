from datetime import datetime, timedelta

userTimeouts = {}
timeoutThreshold = 21

def userTimedOut(name):
	if name in userTimeouts:
		lastTimeout = userTimeouts[name][0]
		repeatTime = timedelta(minutes=timeoutThreshold)
		if datetime.now() - repeatTime < lastTimeout:
			numSuccessiveTimeouts = userTimeouts[name][1] + 1
			userTimeouts[name] = (datetime.now(), numSuccessiveTimeouts)
			return numSuccessiveTimeouts
	
	userTimeouts[name] = (datetime.now(), 1)
	return 1
