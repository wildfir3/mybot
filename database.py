import sqlite3
import config
import os

def initialise(args):
	global db
	print 'Connecting to database file', config.database

	initialiseSchema = False
	if not os.path.exists(config.database):
		print 'Database file does not exist'
		if not args.init_missing_database:
			print 'Error: missing database. Specifiy the --init-missing-database flag if you want to create a blank database at this location'
			exit(1)
		else:
			initialiseSchema = True
	else:
		if args.init_missing_database:
			print 'You requested a new database be created, but we found an existing database. Using existing database'

	db = sqlite3.connect(config.database, detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
	db.row_factory = sqlite3.Row

	if initialiseSchema:
		createDB()

def execute (sql, params):
	tran = db.cursor()
	tran.execute(sql, params)
	db.commit()
	return tran

def applyMigrations():
    print 'Checking for database migrations'
    appliedMigrations = [result[0] for result in execute('select timestampKey from versionInfo', '').fetchall()]

    import os
    import imp
    import inspect
    from migration.migration import Migration
    for filename in os.listdir('migration'):
        modulename, ext = os.path.splitext(filename)
        if modulename[0] != '_' and ext == '.py' and modulename != 'migration':
            module = imp.load_source(modulename, 'migration/' + filename)

            for name, obj in inspect.getmembers(module):
                if inspect.isclass(obj) and name != Migration.__name__:
                    migration = obj()
                    if migration.timestampKey not in appliedMigrations:
                        print 'migration \'%s\', dated %s will be applied' % (name, migration.timestampKey)
                        execute(migration.getSql(), migration.getParams())
                        execute('insert into versionInfo(timestampKey) values (?)', (migration.timestampKey,))

    print 'Database is up to date'

def updatePersistentStore(type, key, value):
	deleteFromPersistentStore(type, key, value)
	addToPersistentStore(type, key, value)

def retrievePersistentStore(type):
	return execute('select key, value from store where type=?', (type,)).fetchall()

def retrieveFromPersistentStore(type, key):
	return execute('select value from store where type=? and key=?', (type, key)).fetchall()

def retrieveOrderedFromPersistentStore(type, key):
	return execute('select value from store where type=? and key=? order by value', (type, key)).fetchall()

def addToPersistentStore(type, key, value):
	execute('insert into store values (?, ?, ?)', (type, key, value))

def deleteFromPersistentStore(type, key, value):
	execute('delete from store where type=? and key=? and value=?', (type, key, value))

def deleteManyFromPersistentStore(type, key):
	execute('delete from store where type=? and key=?', (type, key))

def clearPersistentStore(type):
	execute('delete from store where type=?', (type,))

def createIfNotExists(tableName, columns):
	try:
		db.execute('create table ' + tableName + ' ' + columns)
	except:
		pass

def clearDB ():
	db.execute('drop table versionInfo')
	db.execute('drop table event')
	db.execute('drop table lists')
	db.execute('drop table listmaintainers')
	db.execute('drop table mani')
	db.execute('drop table constants')
	db.execute('drop table gamelists')
	db.execute('drop table userprefs')
	db.execute('drop table store')
	db.execute('drop table tell')
	db.execute('drop table usergroups')
	db.execute('drop table matchChannels')
	db.execute('drop table matches')
	db.execute('drop table matchsignups')
	db.execute('drop table scratchpad')

def createDB ():
	db.execute('create table versionInfo ([id] integer primary key autoincrement, [timestampKey] text)')
	db.execute('create table lists (key text, value text, description text)')
	db.execute('create table listmaintainers (user text, list text)')
	db.execute('create table mani (key text, value text)')
	db.execute('create table constants (key text, value text)')
	db.execute('create table gamelists (key text)')
	db.execute('create table userprefs (user text, key text, value text)')
	db.execute('create table store (type text, key text, value text)')
	db.execute('create table tell (sender text, dateAdded timestamp, recipient text, message text)')
	db.execute('create table usergroups (user text, [group] text)')
	db.execute('create table matchChannels ([id] integer primary key autoincrement, [channel] text, [mode] text, [teamSize] integer)')
	db.execute('create table matches ([id] integer primary key autoincrement, [channelId] integer, [matchId] integer, [date] timestamp, [description] text, [eventId] text)')
	db.execute('create table matchsignups ([matchId] integer, [user] text, [class] integer)')
	db.execute('create table scratchpad ([id] integer primary key autoincrement, [user] text, [date] timestamp, [message] text)')
