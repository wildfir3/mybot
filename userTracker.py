import config
import irc
import ircCommands
from message import Message
from datetime import datetime, timedelta
import handler
import scheduler
import timeUtils

class UserInfo:
	def __init__(self, nick, isRegistered = False, lastSeen = None, online = True, opcode = ''):
		self.nick = nick
		self.isRegistered = isRegistered
		self.lastSeen = lastSeen
		self.lastMessage = ''
		self.online = online
		self.opcode = opcode
		self.quitReason = ''
		self.pastTimes = []
		self.warningLevel = 0
		self.lastWarned = None
		self.context = None

	def isUserRegistered(self):
		if not self.online:
			print 'requested isRegistered for an offline user: ' + self.nick
			return False
		else:
			return self.isRegistered

	def getBanTime(self):
		if self.warningLevel > 1:
			return pow(self.warningLevel - 1, 2)
		return 0

	def getNextBanTime(self):
		if self.warningLevel > 0:
			return pow(self.warningLevel, 2)
		return 0

users = { }
opcodes = {
	'q': '~',
	'a': '&',
	'o': '@',
	'h': '%',
	'v': '+'
}
def userJoined(origUser):
	user = origUser.lower()
	opcode = ''
	if user[0] in opcodes.values():
		opcode = user[0]
		user = user[1:]
		origUser = origUser[1:]
	
	users[user] = UserInfo(origUser, False, datetime.now(), True, opcode)

	if config.enabled('tell'):
		getHandler('tell').userJoined(origUser)

def botJoined(userList):
	for user in userList:
		userJoined(user)

def userLeft(origUser, quitReason):
	user = origUser.lower()
	if user not in users:
		print 'user left who I did not know about: ' + origUser
	else:
		users[user].isRegistered = None
		users[user].online = False
		users[user].lastSeen = datetime.now()
		users[user].quitReason = quitReason

def userChangedName(origUser, newName):
	oldName = origUser.lower()
	if oldName not in users or not users[oldName].online:
		print 'user who changed name didn\'t exit or was not online (presuming not in %s): %s' % (config.channel, origUser)
	else:
		userLeft(oldName, 'changed nickname to \'%s\'' % newName)
		userJoined(newName)
		users[newName.lower()].opcode = users[oldName].opcode
		users[newName.lower()].context = users[oldName].context

def modeChanged(user, mode):
	user = user.lower()
	if user not in users:
		print 'mode change for user I dont know', user
	else:
		if mode.startswith('+'):
			if mode[1] in opcodes:
				users[user].opcode = opcodes[mode[1]]
			else:
				print 'unknown opcode', mode
		else:
			users[user].opcode = ''

def updateLastSeen(origUser, text, checkForSpam = True):
	user = origUser.lower()
	if user not in users:
		print 'updating last seen for a user not online', user
		userJoined(origUser)
	else:
		users[user].lastSeen = datetime.now()
		users[user].lastMessage = text
		users[user].online = True

		if checkForSpam:
			times = users[user].pastTimes
			times.append(datetime.now())
			if len(times) > config.spamCount - 1:
				diff = times[-1] - times[0]
				times.pop(0)
				if diff < timedelta(seconds = config.spamFrequency):
					elevateSpamLevel(user)

def elevateSpamLevel(origUser):
	username = origUser.lower()
	user = users[username]
	if user.lastWarned != None and datetime.now() - user.lastWarned > timedelta(days = config.spamCleanSlateDays):
		user.warningLevel = 0
		if 'logger' in config.handlers:
			getHandler('logger').log('warning level for %s set to 0 - been good for %d days' % (origUser, config.spamCleanSlateDays))

	if user.warningLevel == 0:
		irc.sendMsg(origUser, 'Please don\'t spam the channel. If you want to paste more than 3 lines you can use www.pastebin.com')
	elif user.warningLevel == 1:
		irc.sendMsg(origUser, 'Please do not spam the channel. There will be no further warnings after this time.')
	else:
		tempban(username, datetime.now() + timedelta(minutes=user.getBanTime()))
		irc.inform(config.channel, '%s temporarily silenced for spam (%dm)' % (origUser, user.getBanTime()))
		print origUser, 'spamban for', user.getBanTime(), 'minutes'
	user.warningLevel += 1
	user.pastTimes = []
	user.lastWarned = datetime.now()
	if 'logger' in config.handlers:
		getHandler('logger').log('warning level for %s set to %d' % (origUser, user.warningLevel))

def isAuthedUser(origUser):
	user = origUser.lower()
	if user not in users:
		print 'requested isAuthed for a user I have no data for: ' + origUser
		return irc.isAuthedUser(user)
	else:
		isRegistered = users[user].isUserRegistered()
		if not isRegistered:
			isRegistered = irc.isAuthedUser(user)
			users[user].isRegistered = isRegistered
		return isRegistered

def isOnline(user):
	user = user.lower()
	if user in config.protectedNames:
		return True
	if user not in users:
		return False
	return users[user].online

def isVoiced(user):
	user = user.lower()
	if user not in users:
		return False
	return users[user].opcode == '+'

def removeOfflineUsers(userList):
	return filter(lambda user: isOnline(user), userList)

def getLastSeen(origUser):
	user = origUser.lower()
	if user not in users:
		return 'I haven\'t seen anyone called %s lately' % origUser
	else:
		time = users[user].lastSeen
		diff = datetime.now() - time
		delta = timeUtils.getDelta(diff)

		if users[user].online:
			onlineMessage = '%s is online now!' % users[user].nick
			lastMessage = users[user].lastMessage
			if lastMessage != '':
				onlineMessage += ' %s they said "%s"' % (delta.capitalize(), lastMessage)
			return onlineMessage

		timeSeen = 'I last saw %s%s %s (on %d/%d/%d at %02d:%02d), when they ' % (users[user].opcode, users[user].nick, delta, time.day, time.month, time.year, time.hour, time.minute)

		timeSeen += users[user].quitReason
		return timeSeen

def getUserStatusText(user):
	if user.lower() in users:
		usr = users[user.lower()]
		result = ''
		if usr.online:
			result += 'is online. '
		else:
			result += 'is offline. '
		
		result += 'Anti-spam status: '
		if usr.warningLevel == 0:
			return result + 'In good standing.'
		if usr.warningLevel == 1:
			return result + 'User has received 1 warning (1 warning remains).'
		if usr.warningLevel == 2:
			return result + 'User has received 2 warnings and will be temporarily silenced upon next infraction.'
		else:
			result += 'User will be silenced for ' + str(usr.getNextBanTime()) + 'm next infraction'
		return result
	return None

def tempbanHostname(user, channel, hostname, banExpiryTime):
	if hostname != None:
		if isVoiced(user):
			ircCommands.voice(user, channel, False)
			scheduler.addEvent(banExpiryTime, [ircCommands.voice, user, channel, True])
		ircCommands.ban(hostname, channel, True)
		scheduler.addEvent(banExpiryTime, [ircCommands.ban, hostname, channel, False])

def tempban(user, banExpiryTime):
	hostname = irc.getHostname(user)
	tempbanHostname(user, config.channel, hostname, banExpiryTime)

def cancelTempBan(user, banExpiryTime):
	hostname = irc.getHostname(user)
	ircCommands.ban(hostname, config.channel, False)
	scheduler.removeScheduledEvent(banExpiryTime, [ircCommands.ban, hostname, config.channel, False])

def getHandler(name):
	return handler.getGlobalHandler(name)

def setContext(username, context):
	users[username.lower()].context = context

def getContext(username):
	if username.lower() in users:
		user = users[username.lower()]
		return user.context
	return None

def tryHandleOpenContext(username, message):
	context = getContext(username)
	if context != None:
		context.processUserInput(message)

	return False
