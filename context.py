import irc
import userTracker
from validation import Validation

class ContextManager:

	@staticmethod
	def getContext(username):
		return userTracker.getContext(username)

	@staticmethod
	def setContext(username, context):
		userTracker.setContext(username, context)

	@staticmethod
	def deleteContext(username, deletionReason = '[ContextManager] Your current context has been cleared'):
		userTracker.setContext(username, None)
		if deletionReason != None:
			irc.sendMsg(username, deletionReason)

	@staticmethod
	def resumeContext(username):
		context = ContextManager.getContext(username)
		irc.sendMsg(username, u'[ContextManager] You are already in an %s context. You can clear this context at any time. Just type \u0002cancel\u0002' % context.__class__.__name__)
		context.askNextQuestion()

class RequiredProperty:
	def __init__(self, name, description, validation):
		self.name = name
		self.description = description
		self.validation = validation

	def validate(self, answer):
		return self.validation(answer)

class ContextAware:
	def __init__(self, username):
		self.username = username

	def complete(self):
		return len(self.requiredProperties) == 0
	
	def getCurrentQuestion(self):
		return self.requiredProperties[0]

	def clearCurrentQuestion(self):
		del self.requiredProperties[0]

	def messageUser(self, message):
		irc.sendMsg(self.username, message)

	def askNextQuestion(self):
		irc.sendMsg(self.username, self.getCurrentQuestion().description)

	def userWantsToAbort(self, input):
		return Validation.cancel(input).success

	def processUserInput(self, answer):
		if self.userWantsToAbort(answer):
			ContextManager.deleteContext(self.username)
			return

		property = self.getCurrentQuestion()
		validation = property.validate(answer)
		if validation.success:
			self.__dict__[property.name] = validation.value
			self.clearCurrentQuestion()
		else:
			irc.sendMsg(self.username, validation.failureReason)
			return

		if self.complete():
			ContextManager.deleteContext(self.username, None)
			self.onComplete()
		else:
			self.askNextQuestion()

